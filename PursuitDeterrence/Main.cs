﻿using LSPD_First_Response.Mod.API;
using Rage;
using tanu1215;
using PursuitDeterrence.Utility;
using PursuitDeterrence.Modules;
using PursuitDeterrence.Managers;

namespace PursuitDeterrence
{
    internal class Main : Plugin
    {
        public override void Initialize()
        {
            //Subscribing to our on duty event.
            Functions.OnOnDutyStateChanged += this.OnDutyStateChangedEvent;
        }

        public void OnDutyStateChangedEvent(bool onDuty)
        {

            //This is our logger's prefix.
            Logger.Prefix = "[Pursuit Deterrence]";

            Logger.LogSimple("Version 1.0 loaded.");

            string fileLocation = "plugins/lspdfr/PursuitDeterrence.ini";

            //Here, we setup our configuration / INI file.
            Config.SetupConfigFile(fileLocation);

            //Activate our script when the player goes on duty.
            if (onDuty)
            {
                InitializeModules();
            }
        }

        public override void Finally()
        {
            Logger.LogSimple("Resources cleaned up.");
         
            foreach(GameFiber fiber in FiberManager.GetActiveGameFibers())
            {
                if(fiber.IsAlive)
                {
                    fiber.Abort();
                }
            } 
        }

        private void InitializeModules()
        {
            FiberManager.InitiateFiber(new Deterrence().Run, "DeterrenceFiber");
            //TODO Add reset of modules here.
        }
    }
}