﻿using Rage;
using tanu1215;
using PursuitDeterrence.Utility;

namespace PursuitDeterrence.Managers
{
    /// <summary>
    /// Responsible for handling controller button input.
    /// </summary>
    internal static class ControllerButtonManager
    {
        public static ControllerButtons ControllerButton { get; } = Config.ControllerButtonSetting;

        //Tracks whether the button has been pressed.
        public static bool Pressed { get; set; }

        //Checks for a key press.
        //Gets run in the Main class as a game fiber.
        internal static void CheckButtonPress()
        {
            //Letting the player know that the fiber has been entered.
            Logger.LogSimple("ControllerButtonManager is running!");

            //Constantly runs so we can check key press whenever we want.
            while (true)
            {
                GameFiber.Yield();

                //Checks whether the button has been pressed.
                if ((Game.IsControllerButtonDownRightNow(ControllerButton)))
                {
                    //Debug message letting the user know that the button has been pressed.
                    Logger.LogSimple("Button pressed!");
                    Pressed = true;
                }
            }
        }
    }
}
