﻿using Rage;
using System.Collections.Generic;
using System.Threading;
using tanu1215;
using PursuitDeterrence.Utility;

namespace PursuitDeterrence.Managers
{
    /// <summary>
    /// Responsible for starting fibers.
    /// </summary>
    internal static class FiberManager
    {
        static List<GameFiber> activeFibers = new List<GameFiber>();

        public static void InitiateFiber(ThreadStart fiber, string fiberName)
        {
            if (fiber != null)
            {
                GameFiber newFiber = new GameFiber(fiber, fiberName);
                newFiber.Start();
                activeFibers.Add(newFiber);

                Logger.LogSimple($"Starting {fiberName}.");
            }

            else
            {
                Logger.LogSimple("Fiber is null!");
            }
        }

        public static List<GameFiber> GetActiveGameFibers()
        {
            return activeFibers;
        }

        public static void StopActiveFibers()
        {
            Logger.LogSimple("Stopping all active fibers..");

            int fiberCount = 0;

            foreach (GameFiber fiber in activeFibers)
            {
                fiberCount++;

                Logger.LogSimple($"Stopping {fiber.Name}.");
                fiber.Abort();
            }

            Logger.LogSimple($"Stopped {fiberCount} fibers!");
        }
    }
}
