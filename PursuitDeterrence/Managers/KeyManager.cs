﻿using Rage;
using System.Windows.Forms;
using LSPD_First_Response.Mod.API;
using tanu1215;
using PursuitDeterrence.Utility;

namespace PursuitDeterrence.Managers
{
    /// <summary>
    /// Responsible for handing Key input.
    /// </summary>
    internal static class KeyManager
    {
        //We set our user defined setting here.
        public static Keys KeyBoardKey { get; } = Config.KeySetting;

        //Tracks whether the button has been pressed.
        public static bool Pressed { get; set; }

        //Checks for a key press.
        //Gets run in the Main class as a game fiber.
        //TODO REPLACE WITH EVENT
        internal static void CheckKeyPress()
        {
            //Letting the player know that the fiber has been entered.
            Logger.LogSimple("Keymanager is running!");

            //Constantly runs so we can check key press whenever we want.
            while(true)
            {
                GameFiber.Yield();

                while (Functions.IsPursuitStillRunning(Functions.GetActivePursuit()))
                {
                    GameFiber.Yield();

                    //Checks whether the key has been pressed.
                    if ((Game.IsKeyDownRightNow(KeyBoardKey)))
                    {
                        //Debug message letting the user know that the button has been pressed.
                        Logger.LogSimple($"{KeyBoardKey.ToString()} pressed!");
                        Pressed = true;

                        GameFiber.Sleep(2000);
                    }
                }
            }
        }
    }
}
