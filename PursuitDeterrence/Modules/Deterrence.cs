﻿using System;
using System.Collections.Generic;
using System.Linq;
using LSPD_First_Response.Mod.API;
using Rage;
using tanu1215;
using Rage.Native;
using PursuitDeterrence.Managers;
using PursuitDeterrence.Utility;

namespace PursuitDeterrence.Modules
{
    /// <summary>
    /// Responsible for main logic behind script execution.
    /// </summary>
    internal class Deterrence
    {
        private readonly List<Ped> suspects = new List<Ped>();
        private readonly List<Ped> vehiclePeds = new List<Ped>();
        private readonly List<Vehicle> vehicles = new List<Vehicle>();

        //random variable used to determine whether script will execute successfully.
        private readonly Random random = new Random();

        //Our deterrence's method. This will get executed in the Main class as its own Game Fiber.
        internal void Run()
        {
            //Message letting the user know that this thread has been entered.
            Logger.LogSimple("Deterrence is running!");

            NativeFunction.Natives.SetWeatherTypeNow("WEATHER_TYPE_GOES_HERE");

            //Setting our Player to our own variable so we don't have to 
            //type out Game.LocalPlayer.Character the whole time.
            Ped player = Game.LocalPlayer.Character;

            bool toggledFirstTime = false;

            //Will constantly run, allowing this script to be executed as long as LSPDFR is running.
            while (true)
            {
                GameFiber.Yield();

                Logger.LogSimple (Functions.GetActivePursuit() == null ? "Pursuit is null!" : "Pursuit is NOT null!");

                //This will check if a pursuit is running.
                //If not, wait until one is running so we can proceed.
                while (Functions.GetActivePursuit() == null)
                {
                    GameFiber.Yield();
                }

                //message letting user know that a pursuit is running.
                Logger.LogSimple("Pursuit is Running!");

                //Add all current pursuit suspects to our list.
                suspects.AddRange(Functions.GetPursuitPeds(Functions.GetActivePursuit()).ToList());

                //message allowing player to know that suspects have successfully been added to list.
                Logger.LogSimple("Suspects added to #1");

                //Here, we print to the console all the suspects models in the list.. 
                //Just a way to track and make sure we have what we want.
                suspects.ForEach(x => Logger.LogSimple(x.Model.Name));

                //The ped variable will represent the current Ped / suspect at index 'i'.
                //The vehicle variable represents the ped's vehicle if they are in one.
                Ped ped = null;
                Vehicle vehicle = null;

                //TODO allow for modifying list while running.

                //Below will execute as long as the pursuit is active.
                while ((Functions.GetActivePursuit() != null) &&
                       (Functions.IsPursuitStillRunning(Functions.GetActivePursuit())))
                {
                    GameFiber.Yield();

                    //Checks if our player is in a police vehicle.
                    //If not, then the script won't execute, and it will wait
                    //until our player gets into one.
                    //TODO IMPLEMENT CONTROLLER BUTTON!
                    if (player.IsInAnyPoliceVehicle && Game.IsKeyDownRightNow(Config.KeySetting))
                    {
                        Logger.LogSimple("Key / Button has been pressed... moving on!");

                        int tryCount = 1;

                        if (toggledFirstTime)
                        {
                            tryCount++;
                            GameFiber.Sleep(2000);
                        }

                        else
                        {
                            toggledFirstTime = true;
                        }

                        //Distance between player's police vehicle & suspect vehicle needed to activate script.
                        float distance = 10f;
                        Logger.LogSimple($"Distance of {distance} set!");

                        for (int i = 0; i <= suspects.Count - 1; i++)
                        {
                            Logger.LogSimple("Starting loop.. index " + i + ".");

                            //Sets our global variable to a current index of our suspects.
                            ped = suspects[i];
                            Logger.LogSimple($"Ped at index {i} is model {ped.Model.Name}");

                            //Check if our ped isnt null, does exist, is not arrested, or getting arrested.
                            if (ped && (!Functions.IsPedArrested(ped) || !Functions.IsPedGettingArrested(ped)))
                            {
                                if (ped.IsInAnyVehicle(false))
                                {
                                    //Set our vehicle variable to the ped's current vehicle.
                                    vehicle = ped.CurrentVehicle;

                                    if (!vehicles.Contains(vehicle))
                                    {
                                        vehicles.Add(vehicle);
                                    }

                                    if (vehicle.HasOccupants)
                                    {
                                        //Here, we get all the peds in the vehicle so we can tell them all to surrender.
                                        vehiclePeds.AddRange(vehicle.Occupants);

                                        //We also add them all to the suspect list.
                                        suspects.AddRange(vehiclePeds);

                                        //Check if our player is the determined distance from our suspect.
                                        if (player.Position.DistanceTo(ped.Position) <= distance)
                                        {
                                            Logger.LogSimple($"User's randomness setting = {Config.Randomness}");

                                            //Check for random chance.
                                            if ((random.Next(1, 101)) < 50)
                                            {
                                                //Message, letting player know that script execution was success.
                                                Logger.LogSimple($"Deterring on {ped.Model.Name}. Attempts made = {tryCount}.");

                                                //Remove all their weapons (if any).
                                                //TODO REPLACE WITH RANDOM CHANCES -> WHETHER THEY GIVE UP OR NOT.
                                                vehiclePeds.ForEach(x => x.Inventory.Weapons.Clear());


                                                //We will use this to record the suspect's vehicle's top speed & engine health so we can easily manipulate it later on.
                                                float topSpeed = vehicle.TopSpeed;
                                                float engineHealth = vehicle.EngineHealth;

                                                //Then set the current vehicle's top speed & engine to 0 (to act like the suspect has given up).
                                                vehicle.TopSpeed = 0;
                                                vehicle.EngineHealth = 0;

                                                //Wait until the vehicle's speed hits 0.
                                                while (Functions.IsPursuitStillRunning(Functions.GetActivePursuit()) &&
                                                       vehicle.Speed > 1)
                                                {
                                                    GameFiber.Yield();
                                                }

                                                Logger.LogSimple("Vehicle speed is at 0!");

                                                //Disable the pursuit AI so we can make the suspect do tasks while a part of the pursuit. Note, doesn't always work.
                                                Functions.SetPursuitDisableAI(Functions.GetActivePursuit(), true);

                                                foreach (Ped p in vehiclePeds)
                                                {
                                                    //Tell the ped to leave the vehicle.
                                                    if (p.IsInAnyVehicle(false))
                                                    {
                                                        p.Tasks.LeaveVehicle(LeaveVehicleFlags.LeaveDoorOpen);
                                                    }

                                                    //If the suspect(s) has a weapon in their hands, remove it.
                                                    if (ped.Inventory.EquippedWeapon != null)
                                                    {
                                                        ped.Inventory.EquippedWeapon = WeaponHash.Deer;
                                                    }

                                                    //Tell the ped to put their hands up; surrender!
                                                    ped.Tasks.PutHandsUp(-1, player);
                                                }

                                                //Set the vehicle's speed back, acting as if the vehicle is fine.
                                                vehicle.TopSpeed = topSpeed;
                                                vehicle.EngineHealth = engineHealth;
                                            }
                                            else
                                            {
                                                //Let the player know that the script did not execute successfully.
                                                Logger.LogSimple("Deterring, failed!");
                                                break;
                                            }
                                        }


                                        else
                                        {
                                            Logger.LogSimple("Player is not in range!");
                                            break;
                                        }
                                    }

                                    else
                                    {
                                        Logger.LogSimple("Vehicle is empty!");
                                    }
                                }
                                else
                                {
                                    //Let the player know that the suspect was not in a vehicle.
                                    Logger.LogSimple("Target not in vehicle!");

                                    //if (i != suspects.Count - 1)
                                    //{
                                    //    continue;

                                    //}

                                    //else
                                    //{
                                    //    break;
                                    //}
                                }
                            }
                            else
                            {
                                Logger.LogSimple("Ped is already in cuffs!");
                            }
                        }
                    }
                }
                Logger.LogSimple("Pursuit is over!");

                if (vehicle != null && vehicle.Exists()) vehicle.Dismiss();

                suspects.Clear();
                vehicles.Clear();
                vehiclePeds.Clear();
            }
        }
    }
}