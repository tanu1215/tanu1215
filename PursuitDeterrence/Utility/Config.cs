﻿using Rage;
using System;
using System.Windows.Forms;
using tanu1215;

namespace PursuitDeterrence.Utility
{
    /// <summary>
    /// Responsible for handling user defined settings.
    /// </summary>
    internal static class Config 
    {
        private static InitializationFile ini;

        //The file path for our config file.
        //If this is an LSPDFR plugin (which it is), then we must make sure that
        //the path is pointing into the lspdfr folder under the plugins directory.

        /// <summary>
        /// Gets the desired key setting.
        /// </summary>
        internal static Keys KeySetting => ini.ReadEnum<Keys>("Bindings", "KeyBoardKey", Keys.LShiftKey);

        /// <summary>
        /// Gets the desired controller button setting.
        /// </summary>
        internal static ControllerButtons ControllerButtonSetting
        {
            get
            {
                Logger.LogSimple("GetControllerSetting()");

                //Here, we get our Controller setting. If nothing is given, then the X button will be set.
                return ini.ReadEnum<ControllerButtons>("Bindings", "ControllerButton", ControllerButtons.X);
            }
        }

        /// <summary>
        /// Gets whether the script is in debug mode.
        /// Not implemented yet.
        /// </summary>
        internal static bool IsInDebug
        {
            get
            {
                Logger.LogSimple("GetDebugEnabled()");

                //A debug option.. used for developer mode.
                //Will print more messages to screen.
                return ini.ReadBoolean("Debug", "IsDebug", false);
            }
        }

        /// <summary>
        /// Gets the % likeliness that the script will execute successfully.
        /// Overriden by the GUI version.
        /// </summary>
        internal static int Randomness
        {
            get
            {
                Logger.LogSimple("GetRandomness()");

                // 1- 101 (100 %)

                Logger.LogSimple(ini.ReadInt32("Randomness", "RandomChance").ToString());
                return ini.ReadInt32("Randomness", "RandomChance", 25);
            }
        }

        /// <summary>
        /// Gets whether the GUI version of the script is enabled.
        /// Overides the % version.
        /// </summary>
        internal static bool IsGuiEnabled
        {
            get
            {
                Logger.LogSimple("GetGuiEnabled()");

                return ini.ReadBoolean("GUI", "Enabled", false);
            }
        }

        //Setting up our config file.
        //Called in the Main class.
        internal static void SetupConfigFile(string fileLocation)
        {
            if (ini == null)
            {
                try
                {
                    //Creating the ini file in the defined location.
                    ini = new InitializationFile(fileLocation);

                    if (!ini.Exists())
                    {

                        ini.Create();
                        Logger.LogSimple($"INI generated in {fileLocation}.");
                    }

                    else
                    {
                        Logger.LogSimple($"Error, INI already in {fileLocation}.");
                    }
                }

                catch (Exception ex)
                {
                    Logger.LogSimple("Error generating INI! " + ex.ToString());
                }
            }
        }
    }
}